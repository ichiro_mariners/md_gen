#!/usr/bin/env python3

import sys
import os
import argparse

import add_sys_path

from md_lib.file_container import FileContainer
from md_lib.png_ref import enable_pu_in_comment


def get_args(args=None):
    parser = argparse.ArgumentParser(description="to enable pu code in html comments")
    parser.add_argument("md", nargs=1)
    parser.add_argument("-o", nargs=1)

    args = parser.parse_args(args)

    return {"md": args.md[0], "o": args.o[0]}


if __name__ == "__main__":
    args = get_args()
    fc = enable_pu_in_comment(FileContainer(args["md"]), args["o"])
    fc.save()
