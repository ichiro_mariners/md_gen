import re
import base64
import pprint
from os import path

from md_lib.file_container import FileContainer, related_path_in_md


_REF_PU_PNG = re.compile(r"^!\[[^\]]+\]\((?P<png>[^\)]+\.png)\)\n")


def get_png_in_link(md_file_path: str, line: str) -> str:
    if match_png := _REF_PU_PNG.search(line):
        return related_path_in_md(md_file_path, match_png.groupdict()["png"])

    return None


def _image2base64(path):
    with open(path, "rb") as f:
        return base64.b64encode(f.read()).decode()


def _convert_to_html_comment_safe(content):
    ret = []

    for pu_code in content:
        ret.append(pu_code.replace("-->", "@@>"))

    return ret


def _revert_from_html_comment_safe(pu_code):
    return pu_code.replace("@@>", "-->")


def _pu_src_commend(png):
    pu = png.replace("png", "pu")

    if not path.exists(pu):
        return None

    pu_fc = FileContainer(pu)

    lines = [f"<!-- plantuml {pu}\n", "```\n", f"' plantuml {pu}\n"]

    content = _convert_to_html_comment_safe(pu_fc.content)

    for line in content:
        lines.append(f"     {line}")

    lines += ["```\n", "-->\n"]

    return lines


def enable_pu_in_comment(fc: FileContainer, new_md_file: str) -> FileContainer:
    pu_in_comment_beg_re = re.compile(r"^<!-- plantuml ")
    pu_in_comment_end_re = re.compile(r"^-->")
    new_content = []

    in_pu_in_comment = False
    for line in fc.content:
        if pu_in_comment_beg_re.match(line):
            in_pu_in_comment = True
            continue

        if in_pu_in_comment and pu_in_comment_end_re.match(line):
            in_pu_in_comment = False
            continue

        if in_pu_in_comment:
            line = _revert_from_html_comment_safe(line)

        new_content.append(line)

    return FileContainer(new_md_file, new_content)


def change_png_link_to_base64(fc: FileContainer) -> FileContainer:
    new_content = []

    for line in fc.content:
        if match_png := _REF_PU_PNG.match(line):
            png = related_path_in_md(fc.filename, match_png.groupdict()["png"])
            base64 = _image2base64(png)

            line = f'<p><img src="data:image/png;base64,{base64}" /></p>\n'

            if comment := _pu_src_commend(png):
                new_content += comment

        new_content.append(line)

    return FileContainer(fc.filename, new_content)
